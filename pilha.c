#include <stdio.h>
#include <stdlib.h>
#include "pilha.h"


Pilha *pilhaCria(){

	Pilha *p = (Pilha*)malloc(sizeof(Pilha));
	p->prim = NULL;
	return p;
}

void pilhaPush(Pilha *p,float v){

	ListaPilha *n = (ListaPilha *)malloc(sizeof(ListaPilha));
	n->info = v;
	n->prox = p->prim;
	p->prim = n;
}

float pilhaPop(Pilha *p){

	ListaPilha *t;
	float v;
	if(pilhaVazia(p)){

		return -1;
	}
	t = p->prim;
	v = t->info;
	p->prim = t->prox;
	free(t);
	return v;
}

int pilhaVazia(Pilha *p){
	return (p->prim == NULL);
}

void pilhaLibera(Pilha *p){

	ListaPilha *q = p->prim;
	while(q != NULL){
		ListaPilha *t = q->prox;
		free(q);
		q = t;
	}

	free(q);
}


void pilhaImprime(Pilha *p){

	ListaPilha *q;
	for(q = p->prim;q != NULL;q = q->prox)
		printf("%d ",q->info);
	printf("\n");
	printf("\n");
}