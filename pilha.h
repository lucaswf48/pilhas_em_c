struct listapilha{
	int info;
	struct lista *prox;
};

typedef struct listapilha ListaPilha;

struct pilha{

	ListaPilha *prim;
};

typedef struct pilha Pilha;

Pilha *pilhaCria();
void pilhaPush(Pilha *p,float v);
float pilhaPop(Pilha *p);
int pilhaVazia(Pilha *p);
void pilhaLibera(Pilha *p);
void pilhaImprime(Pilha *p);

